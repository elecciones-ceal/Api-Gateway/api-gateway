import { Module } from '@nestjs/common';
import { CealModule } from './ceals/ceal.module';
import { ConfigModule } from '@nestjs/config';
import { VoteModule } from './vote/vote.module';

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
    }),
    CealModule,
    VoteModule
  ],  
})
export class AppModule {}
