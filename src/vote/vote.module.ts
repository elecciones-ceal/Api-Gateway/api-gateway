import { Module } from '@nestjs/common';
import { VoteController } from './vote.controller';
import { VoteService } from './vote.service';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { ClientsModule, Transport } from '@nestjs/microservices';
import { VOTING_PACKAGE_NAME, VOTING_SERVICE_NAME } from './vote.pb';

@Module({
  imports: [
    ConfigModule,
    ClientsModule.registerAsync([
      {
        name: VOTING_SERVICE_NAME,
        imports: [ConfigModule],
        useFactory: async (configService: ConfigService) => ({
          transport: Transport.GRPC,
          options: {
            url: configService.get<string>('MS_VOTE_URL'),
            package: VOTING_PACKAGE_NAME,
            protoPath: 'node_modules/shared-protos/proto/vote.proto',
          },
        }),
        inject: [ConfigService],
      },
    ]),
  ],

  controllers: [VoteController],
  providers: [VoteService],
  exports: [VoteService],

})
export class VoteModule {}
