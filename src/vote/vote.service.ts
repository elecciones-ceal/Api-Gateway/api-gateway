import { Injectable, Inject } from '@nestjs/common';
import { ClientGrpc } from '@nestjs/microservices';
import { VOTING_SERVICE_NAME, VotingServiceClient } from './vote.pb';

@Injectable()
export class VoteService {
  private svc: VotingServiceClient;

  @Inject(VOTING_SERVICE_NAME)
  private readonly client: ClientGrpc;

  public onModuleInit(): void {
    this.svc = this.client.getService<VotingServiceClient>(VOTING_SERVICE_NAME);
  }
}
