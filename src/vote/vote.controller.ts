import {
    Body,
    Controller,
    Delete,
    Get,
    Inject,
    OnModuleInit,
    Param,
    Post,
    Put,
  } from '@nestjs/common';
  import {
    VOTING_SERVICE_NAME,
    VotingServiceClient,
    CreateElectionRequest,
    GetElectionRequest,
    CreateVoteCountRequest,
    GetVoteCountRequest,
    CreateParticipantRequest,
    GetParticipantRequest,
    Empty,
    Election,
    ElectionList,
    VoteCount,
    VoteCountList,
    Participant,
    ParticipantList,
  } from './vote.pb';
  import { ClientGrpc } from '@nestjs/microservices';
  import { Observable } from 'rxjs';
  
  @Controller('vote')
  export class VoteController implements OnModuleInit {
    private svc: VotingServiceClient;
  
    @Inject(VOTING_SERVICE_NAME)
    private readonly client: ClientGrpc;
  
    public onModuleInit(): void {
      this.svc = this.client.getService<VotingServiceClient>(VOTING_SERVICE_NAME);
    }
  
    @Post('election')
    createElection(@Body() body: CreateElectionRequest): Observable<Election> {
      return this.svc.createElection(body);
    }
  
    @Get('election/:id')
    getElection(@Param('id') id: number): Observable<Election> {
      return this.svc.getElection({ id });
    }
  
    @Get('elections')
    listElections(): Observable<ElectionList> {
      return this.svc.listElections({});
    }
  
    @Post('vote-count')
    createVoteCount(@Body() body: CreateVoteCountRequest): Observable<VoteCount> {
      return this.svc.createVoteCount(body);
    }
  
    @Get('vote-count/:id')
    getVoteCount(@Param('id') id: number): Observable<VoteCount> {
      return this.svc.getVoteCount({ id });
    }
  
    @Get('vote-counts')
    listVoteCounts(): Observable<VoteCountList> {
      return this.svc.listVoteCounts({});
    }
  
    @Post('participant')
    createParticipant(@Body() body: CreateParticipantRequest): Observable<Participant> {
      return this.svc.createParticipant(body);
    }
  
    @Get('participant/:id')
    getParticipant(@Param('id') id: number): Observable<Participant> {
      return this.svc.getParticipant({ id });
    }
  
    @Get('participants')
    listParticipants(): Observable<ParticipantList> {
      return this.svc.listParticipants({});
    }
  }
  