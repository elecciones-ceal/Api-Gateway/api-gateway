import { Global, Module } from '@nestjs/common';
import { CealController } from './ceal.controller';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { ClientsModule, Transport } from '@nestjs/microservices';
import { CEAL_PACKAGE_NAME, LIST_SERVICE_NAME } from './ceal.pb';
import { CealService } from './ceal.service';

@Global()
@Module({
  imports: [
    ConfigModule,
    ClientsModule.registerAsync([
      {
        name: LIST_SERVICE_NAME,
        imports: [ConfigModule],
        useFactory: async (configService: ConfigService) => ({
          transport: Transport.GRPC,
          options: {
            url: configService.get<string>('MS_CEAL_URL'),
            package: CEAL_PACKAGE_NAME,
            protoPath: 'node_modules/shared-protos/proto/ceal.proto',
          },
        }),
        inject: [ConfigService],
      },
    ]),
  ],
  controllers: [CealController],
  providers: [CealService],
  exports: [CealService],
})
export class CealModule {}