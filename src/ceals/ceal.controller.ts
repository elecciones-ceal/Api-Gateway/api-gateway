import {
  Body,
  Controller,
  Delete,
  Get,
  Inject,
  OnModuleInit,
  Param,
  Post,
  Put,
} from '@nestjs/common';
import {
  AddIntegrantToListRequest,
  AllListsResponse,
  CreateListRequest,
  DeleteIntegrantFromListRequest,
  DeleteIntegrantResponse,
  DeleteListRequest,
  DeleteListResponse,
  ElectionIdRequest,
  GetAllListsRequest,
  GetIntegrantsByListRequest,
  GetListRequest,
  IntegrantResponse,
  IntegrantsResponse,
  LIST_SERVICE_NAME,
  ListResponse,
  ListServiceClient,
  UpdateListRequest,
} from './ceal.pb';
import { ClientGrpc } from '@nestjs/microservices';
import { Observable } from 'rxjs';

@Controller('ceal')
export class CealController implements OnModuleInit {
  public onModuleInit(): void {
    this.svc = this.client.getService<ListServiceClient>(LIST_SERVICE_NAME);
  }

  private svc: ListServiceClient;

  @Inject(LIST_SERVICE_NAME)
  private readonly client: ClientGrpc;

  public OnModuleInit(): void {
    this.svc = this.client.getService<ListServiceClient>(LIST_SERVICE_NAME);
  }

  @Post('create-list')
  async createList(@Body() body: CreateListRequest) {
    return this.svc.createList(body);
  }

  @Get('get-all-lists')
  getAllLists(): Observable<AllListsResponse> {
    const request: GetAllListsRequest = {};
    return this.svc.getAllLists(request);
  }

  @Get('get-list/:id')
  getList(@Param('id') id: number): Observable<ListResponse> {
    const request: GetListRequest = { id };
    return this.svc.getList(request);
  }

  @Put('update-list/:id')
  updateList(
    @Param('id') id: number,
    @Body() body: UpdateListRequest,
  ): Observable<ListResponse> {
    const request: UpdateListRequest = { ...body, id };
    return this.svc.updateList(request);
  }

  @Delete('delete-list/:id')
  deleteList(@Param('id') id: number): Observable<DeleteListResponse> {
    const request: DeleteListRequest = { id };
    return this.svc.deleteList(request);
  }

  @Post('add-integrant')
  addIntegrantToList(
    @Body() body: AddIntegrantToListRequest,
  ): Observable<IntegrantResponse> {
    return this.svc.addIntegrantToList(body);
  }

  @Get('get-integrants/:idList')
  getIntegrantsByList(
    @Param('idList') idList: number,
  ): Observable<IntegrantsResponse> {
    const request: GetIntegrantsByListRequest = { idList };
    return this.svc.getIntegrantsByList(request);
  }

  @Delete('delete-integrant')
  deleteIntegrantFromList(
    @Body() body: DeleteIntegrantFromListRequest,
  ): Observable<DeleteIntegrantResponse> {
    return this.svc.deleteIntegrantFromList(body);
  }

  @Get('get-lists-by-election/:electionId')
  getListsByElectionId(
    @Param('electionId') electionId: number,
  ): Observable<AllListsResponse> {
    const request: ElectionIdRequest = { electionId }; 
    return this.svc.getListsByElectionId(request);
  }

}
