import { Injectable, Inject } from '@nestjs/common';
import { ClientGrpc } from '@nestjs/microservices';
import { LIST_SERVICE_NAME, ListServiceClient } from './ceal.pb';

@Injectable()
export class CealService {
  private svc: ListServiceClient;

  @Inject(LIST_SERVICE_NAME)
  private readonly client: ClientGrpc;

  public onModuleInit(): void {
    this.svc = this.client.getService<ListServiceClient>(LIST_SERVICE_NAME);
  }
}
