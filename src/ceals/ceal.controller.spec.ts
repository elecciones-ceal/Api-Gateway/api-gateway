import { Test, TestingModule } from '@nestjs/testing';
import { CealController } from './ceal.controller';
import {
  ClientsModule,
  Transport,
  ClientGrpcProxy,
} from '@nestjs/microservices';
import { ConfigModule, ConfigService } from '@nestjs/config';
import {
  AddIntegrantToListRequest,
  AllListsResponse,
  CreateListRequest,
  DeleteIntegrantFromListRequest,
  DeleteIntegrantResponse,
  DeleteListRequest,
  DeleteListResponse,
  ElectionIdRequest,
  GetIntegrantsByListRequest,
  GetListRequest,
  IntegrantResponse,
  IntegrantsResponse,
  LIST_SERVICE_NAME,
  ListResponse,
  UpdateListRequest,
} from './ceal.pb';
import { of, throwError } from 'rxjs';

describe('CealController', () => {
  let controller: CealController;
  let listServiceClient: {
    createList: jest.Mock;
    getAllLists: jest.Mock;
    getList: jest.Mock;
    updateList: jest.Mock;
    deleteList: jest.Mock;
    addIntegrantToList: jest.Mock;
    getIntegrantsByList: jest.Mock;
    deleteIntegrantFromList: jest.Mock;
    getListsByElectionId: jest.Mock;
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [
        ConfigModule.forRoot(),
        ClientsModule.registerAsync([
          {
            name: LIST_SERVICE_NAME,
            imports: [ConfigModule],
            useFactory: async (configService: ConfigService) => ({
              transport: Transport.GRPC,
              options: {
                url: configService.get<string>('MS_CEALS_URL'),
                package: 'ceal',
                protoPath:
                  'node_modules/shared-protos-mytricel/proto/ceal.proto',
              },
            }),
            inject: [ConfigService],
          },
        ]),
      ],
      controllers: [CealController],
    }).compile();

    controller = module.get<CealController>(CealController);
    listServiceClient = {
      createList: jest.fn(),
      getAllLists: jest.fn(),
      getList: jest.fn(),
      updateList: jest.fn(),
      deleteList: jest.fn(),
      addIntegrantToList: jest.fn(),
      getIntegrantsByList: jest.fn(),
      deleteIntegrantFromList: jest.fn(),
      getListsByElectionId: jest.fn(),
    };

    const clientGrpcProxy = module.get<ClientGrpcProxy>(LIST_SERVICE_NAME);
    jest
      .spyOn(clientGrpcProxy, 'getService')
      .mockReturnValue(listServiceClient);

    controller.onModuleInit();
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  describe('createList', () => {
    it('should call listServiceClient.createList and return the result', async () => {
      const createListRequest: CreateListRequest = {
        name: 'Test List',
        description: 'Description',
        electionId: 1,
      };
      const listResponse: ListResponse = {
        id: 1,
        name: 'Test List',
        description: 'Description',
        electionId: 1,
      };

      listServiceClient.createList.mockReturnValue(of(listResponse));

      const result = await controller.createList(createListRequest);
      result.subscribe((response) => {
        expect(response).toEqual(listResponse);
      });

      expect(listServiceClient.createList).toHaveBeenCalledWith(
        createListRequest,
      );
    });
  });

  describe('getAllLists', () => {
    it('should call listServiceClient.getAllLists and return the result', async () => {
      const allListsResponse: AllListsResponse = { lists: [] };

      listServiceClient.getAllLists.mockReturnValue(of(allListsResponse));

      const result = await controller.getAllLists();
      result.subscribe((response) => {
        expect(response).toEqual(allListsResponse);
      });

      expect(listServiceClient.getAllLists).toHaveBeenCalledWith({});
    });
  });

  describe('getList', () => {
    it('should call listServiceClient.getList and return the result', async () => {
      const getListRequest: GetListRequest = { id: 1 };
      const listResponse: ListResponse = {
        id: 1,
        name: 'Test List',
        description: 'Description',
        electionId: 1,
      };

      listServiceClient.getList.mockReturnValue(of(listResponse));

      const result = await controller.getList(1);
      result.subscribe((response) => {
        expect(response).toEqual(listResponse);
      });

      expect(listServiceClient.getList).toHaveBeenCalledWith(getListRequest);
    });
  });

  describe('updateList', () => {
    it('should call listServiceClient.updateList and return the result', async () => {
      const updateListRequest: UpdateListRequest = {
        id: 1,
        name: 'Updated List',
        description: 'Updated Description',
        electionId: 1,
      };
      const listResponse: ListResponse = {
        id: 1,
        name: 'Updated List',
        description: 'Updated Description',
        electionId: 1,
      };

      listServiceClient.updateList.mockReturnValue(of(listResponse));

      const result = await controller.updateList(1, updateListRequest);
      result.subscribe((response) => {
        expect(response).toEqual(listResponse);
      });

      expect(listServiceClient.updateList).toHaveBeenCalledWith(
        updateListRequest,
      );
    });
  });

  describe('deleteList', () => {
    it('should call listServiceClient.deleteList and return the result', async () => {
      const deleteListRequest: DeleteListRequest = { id: 1 };
      const deleteListResponse: DeleteListResponse = { success: true };

      listServiceClient.deleteList.mockReturnValue(of(deleteListResponse));

      const result = await controller.deleteList(1);
      result.subscribe((response) => {
        expect(response).toEqual(deleteListResponse);
      });

      expect(listServiceClient.deleteList).toHaveBeenCalledWith(
        deleteListRequest,
      );
    });
  });

  describe('addIntegrantToList', () => {
    it('should call listServiceClient.addIntegrantToList and return the result', async () => {
      const addIntegrantToListRequest: AddIntegrantToListRequest = {
        idList: 1,
        idUser: 1,
        electionId: 1,
      };
      const integrantResponse: IntegrantResponse = {
        id: 1,
        idList: 1,
        idUser: 1,
      };

      listServiceClient.addIntegrantToList.mockReturnValue(
        of(integrantResponse),
      );

      const result = await controller.addIntegrantToList(
        addIntegrantToListRequest,
      );
      result.subscribe((response) => {
        expect(response).toEqual(integrantResponse);
      });

      expect(listServiceClient.addIntegrantToList).toHaveBeenCalledWith(
        addIntegrantToListRequest,
      );
    });
  });

  describe('getIntegrantsByList', () => {
    it('should call listServiceClient.getIntegrantsByList and return the result', async () => {
      const getIntegrantsByListRequest: GetIntegrantsByListRequest = {
        idList: 1,
      };
      const integrantsResponse: IntegrantsResponse = { integrants: [] };

      listServiceClient.getIntegrantsByList.mockReturnValue(
        of(integrantsResponse),
      );

      const result = await controller.getIntegrantsByList(1);
      result.subscribe((response) => {
        expect(response).toEqual(integrantsResponse);
      });

      expect(listServiceClient.getIntegrantsByList).toHaveBeenCalledWith(
        getIntegrantsByListRequest,
      );
    });
  });

  describe('deleteIntegrantFromList', () => {
    it('should call listServiceClient.deleteIntegrantFromList and return the result', async () => {
      const deleteIntegrantFromListRequest: DeleteIntegrantFromListRequest = {
        idList: 1,
        idUser: 1,
      };
      const deleteIntegrantResponse: DeleteIntegrantResponse = {
        success: true,
      };

      listServiceClient.deleteIntegrantFromList.mockReturnValue(
        of(deleteIntegrantResponse),
      );

      const result = await controller.deleteIntegrantFromList(
        deleteIntegrantFromListRequest,
      );
      result.subscribe((response) => {
        expect(response).toEqual(deleteIntegrantResponse);
      });

      expect(listServiceClient.deleteIntegrantFromList).toHaveBeenCalledWith(
        deleteIntegrantFromListRequest,
      );
    });
  });

  describe('getListsByElectionId', () => {
    it('should call listServiceClient.getListsByElectionId and return the result', async () => {
      const electionId = 1;
      const expectedResponse: AllListsResponse = {
        lists: [
          { id: 1, name: 'List 1', description: 'Description 1', electionId: 1 },
          { id: 2, name: 'List 2', description: 'Description 2', electionId: 1 },
        ],
      };

      listServiceClient.getListsByElectionId.mockReturnValue(of(expectedResponse));

      const result = await controller.getListsByElectionId(electionId);
      result.subscribe((response) => {
        expect(response).toEqual(expectedResponse);
      });

      expect(listServiceClient.getListsByElectionId).toHaveBeenCalledWith(
        { election_id: electionId },
      );
    });
  });
});
